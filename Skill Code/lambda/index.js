/* eslint-disable  func-names */
/* eslint-disable  no-console */
/* eslint-disable  no-restricted-syntax */

// IMPORTANT: Please note that this template uses Dispay Directives,
// Display Interface for your skill should be enabled through the Amazon developer console
// See this screenshot - https://alexa.design/enabledisplay
const Alexa = require('ask-sdk-core');

/* INTENT HANDLERS */
const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return handlerInput.requestEnvelope.request.type === `LaunchRequest`;
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder
      .speak(welcomeMessage)
      .reprompt(helpMessage)
      .withShouldEndSession(false)
      .getResponse();
  },
};

const MatchHandler = {
    canHandle(handlerInput){
        const request = handlerInput.requestEnvelope.request;
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        console.log("Inside MatchHandler");
        console.log(JSON.stringify(request));
        return request.type === "IntentRequest" &&
           (request.intent.name === "GameIntent" && undefined==attributes.state);
    },
    handle(handlerInput){
        console.log("Inside MatchHandler - handle");
            const attributes = handlerInput.attributesManager.getSessionAttributes();
            //const response = handlerInput.responseBuilder;
            attributes.skill = 0.329; // the percentage chance a user has to initially make a shot
            attributes.shotsMade = 0; // shots made
            attributes.shotsMissed = 0; // shots missed
            attributes.level = 0; // initial difficulty level
            attributes.state = states.MATCH;
            var speakOutput = attemptShot(handlerInput);
            handlerInput.attributesManager.setSessionAttributes(attributes);
            return handlerInput.responseBuilder
                .speak(speakOutput+" Would you like to take another shot?")
                .reprompt(helpMessage)
                .withShouldEndSession(false)
                .getResponse();
    },
};

const AnotherMatchHandler = {
    canHandle(handlerInput){
        const request = handlerInput.requestEnvelope.request;
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        console.log("Inside AnotherMatchHandler");
        console.log(JSON.stringify(request));
        return request.type === "IntentRequest" &&
           (request.intent.name === "AnotherIntent" && attributes.state===states.MATCH);
    },
    handle(handlerInput){
        console.log("Inside AnotherMatchHandler - handle");
            const attributes = handlerInput.attributesManager.getSessionAttributes();
            var speakOutput = attemptShot(handlerInput);
            attributes.state = states.MATCH;
            handlerInput.attributesManager.setSessionAttributes(attributes);
            return handlerInput.responseBuilder
                .speak(speakOutput+" Would you like to take another shot?")
                .reprompt(helpMessage)
                .withShouldEndSession(false)
                .getResponse();
    },
};

const TransferHandler = {
    canHandle(handlerInput){
        const request = handlerInput.requestEnvelope.request;
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        console.log("Inside TransferHandler");
        console.log(JSON.stringify(request));
        return request.type === "IntentRequest" &&
           (request.intent.name === "GameIntent" && attributes.state===states.QUIZ);
    },
    handle(handlerInput){
        console.log("Inside TransferMatchHandler - handle");
            const attributes = handlerInput.attributesManager.getSessionAttributes();
            var speakOutput = attemptShot(handlerInput);
            attributes.state = states.MATCH;
            handlerInput.attributesManager.setSessionAttributes(attributes);
            return handlerInput.responseBuilder
                .speak(speakOutput+" Would you like to take another shot?")
                .reprompt(helpMessage)
                .withShouldEndSession(false)
                .getResponse();
    },
};

const QuizHandler = {
    canHandle(handlerInput){
        const request = handlerInput.requestEnvelope.request;
        console.log("Inside QuizHandler");
        console.log(JSON.stringify(request));
        return request.type === "IntentRequest" &&
           (request.intent.name === "QuizIntent");
    },
    handle(handlerInput){
        console.log("Inside QuizHandler - handle");
            const attributes = handlerInput.attributesManager.getSessionAttributes();
            attributes.state = states.QUIZ;
            handlerInput.attributesManager.setSessionAttributes(attributes);
            var question = askQuestion(handlerInput);
            var speakOutput = startQuizMessage + question;
            var repromptOutput = question;
            
            return handlerInput.responseBuilder
                .speak(speakOutput)
                .reprompt(repromptOutput)
                .withShouldEndSession(false)
                .getResponse();
    },
};

const AnotherQuizHandler = {
    canHandle(handlerInput){
        const request = handlerInput.requestEnvelope.request;
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        console.log("Inside AnotherQuizHandler");
        console.log(JSON.stringify(request));
        return request.type === "IntentRequest" &&
           (request.intent.name === "AnotherIntent" && attributes.state===states.QUIZ);
    },
    handle(handlerInput){
        console.log("Inside QuizHandler - handle");
            const attributes = handlerInput.attributesManager.getSessionAttributes();
            attributes.state = states.QUIZ;
            handlerInput.attributesManager.setSessionAttributes(attributes);
            var question = askQuestion(handlerInput);
            var speakOutput = question;
            var repromptOutput = question;
            
            return handlerInput.responseBuilder
                .speak(speakOutput)
                .reprompt(repromptOutput)
                .withShouldEndSession(false)
                .getResponse();
    },
};

const QuizAnswerHandler = {
    canHandle(handlerInput){
        const request = handlerInput.requestEnvelope.request;
        console.log("Inside AnswerQuizHandler");
        console.log(JSON.stringify(request));
        return request.type === "IntentRequest" &&
           (request.intent.name === "AnswerIntent");
    },
    handle(handlerInput){
        const attributes = handlerInput.attributesManager.getSessionAttributes();
        console.log("Inside AnswerQuizHandler - handle");
        attributes.state = states.QUIZ;
        handlerInput.attributesManager.setSessionAttributes(attributes);
        //var isCorrect = compare(handlerInput.requestEnvelope.request.intent.slots, attributes.idex);
        var isCorrect = true;
        var result;
        if(isCorrect){
            result = "Correct!"
            attributes.skill += .100;
            handlerInput.attributesManager.setSessionAttributes(attributes);
        }else{
                result = "Incorrect."
        }
        var speakOutput = result + " Would you like to answer another question or attempt a shot?"
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(speakOutput)
            .withShouldEndSession(false)
            .getResponse();
    },
};


/*const RepeatHandler = {
  canHandle(handlerInput) {
    console.log("Inside RepeatHandler");
    const attributes = handlerInput.attributesManager.getSessionAttributes();
    const request = handlerInput.requestEnvelope.request;

    return request.type === 'IntentRequest' &&
           request.intent.name === 'MatchHandler';
  },
  handle(handlerInput) {
    console.log("Inside RepeatHandler - handle");
    const attributes = handlerInput.attributesManager.getSessionAttributes();
    var speakOutput = attemptShot(handlerInput)
    handlerInput.attributesManager.setSessionAttributes(attributes);
    return handlerInput.responseBuilder
        .speak(speakOutput)
        .reprompt(speakOutput)
        .withShouldEndSession(false)
        .getResponse();
  },
};*/

/*const HelpHandler = {
  canHandle(handlerInput) {
    console.log("Inside HelpHandler");
    const request = handlerInput.requestEnvelope.request;
    return request.type === 'IntentRequest' &&
           request.intent.name === 'AMAZON.HelpHandler';
  },
  handle(handlerInput) {
    console.log("Inside HelpHandler - handle");
    return handlerInput.responseBuilder
      .speak(helpMessage)
      .reprompt(helpMessage)
      .getResponse();
  },
};*/

const ExitHandler = {
  canHandle(handlerInput) {
    console.log("Inside ExitHandler");
    const attributes = handlerInput.attributesManager.getSessionAttributes();
    const request = handlerInput.requestEnvelope.request;

    return request.type === `IntentRequest` && (
              request.intent.name === 'AMAZON.StopIntent' ||
              request.intent.name === 'AMAZON.PauseIntent' ||
              request.intent.name === 'AMAZON.CancelIntent'
           );
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder
      .speak(exitSkillMessage)
      .getResponse();
  },
};

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    console.log("Inside SessionEndedRequestHandler");
    return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    console.log(`Session ended with reason: ${JSON.stringify(handlerInput.requestEnvelope)}`);
    return handlerInput.responseBuilder.getResponse();
  },
};

/*const ErrorHandler = {
  canHandle() {
    console.log("Inside ErrorHandler");
    return true;
  },
  handle(handlerInput, error) {
    console.log("Inside ErrorHandler - handle");
    console.log(`Error handled: ${JSON.stringify(error)}`);
    console.log(`Handler Input: ${JSON.stringify(handlerInput)}`);

    return handlerInput.responseBuilder
      .speak(helpMessage)
      .reprompt(helpMessage)
      .getResponse();
  },
};*/

/* CONSTANTS */
const skillBuilder = Alexa.SkillBuilders.custom();
const speechConsCorrect = ['Booya', 'All righty', 'Bam', 'Bazinga', 'Bingo', 'Boom', 'Bravo', 'Cha Ching', 'Cheers', 'Dynomite', 'Hip hip hooray', 'Hurrah', 'Hurray', 'Huzzah', 'Oh dear.  Just kidding.  Hurray', 'Kaboom', 'Kaching', 'Oh snap', 'Phew','Righto', 'Way to go', 'Well done', 'Whee', 'Woo hoo', 'Yay', 'Wowza', 'Yowsa'];
const speechConsWrong = ['Argh', 'Aw man', 'Blarg', 'Blast', 'Boo', 'Bummer', 'Darn', "D'oh", 'Dun dun dun', 'Eek', 'Honk', 'Le sigh', 'Mamma mia', 'Oh boy', 'Oh dear', 'Oof', 'Ouch', 'Ruh roh', 'Shucks', 'Uh oh', 'Wah wah', 'Whoops a daisy', 'Yikes'];

const welcomeMessage = `Welcome to Clear Out!  You can attempt a shot, ask about your player stats, or boost your shooting skill by answering trivia questions.  What would you like to do?`;
const startQuizMessage = `OK.  I will ask you questions about Kobe Bryant. `;
const exitSkillMessage = `Thank you for playing Clear Out!  Let's play again soon, Mamba out!`;
const repromptSpeech = `reprompt`;
const helpMessage = `You can ask me about your player stats, and I'll tell you what I know.  You can also test your shooting ability by attempting a basket. Lastly, you may boost your shooting ability by answering a trivia question correctly.  What would you like to do?`;
const useCardsFlag = true;

const states = {
    MATCH: `_MATCH`,
    QUIZ: `_QUIZ`,
};

const data = [
    {Question: 'What year did Kobe Bryant get drafted in the NBA?', Answer: '1996'},
    {Question: 'What pick in the draft was Kobe Bryant?', Answer: '13'},
    {Question: 'What NBA team drafted Kobe Bryant?', Answer: 'Charlotte Hornets'},
    {Question: 'What NBA team traded for Kobe Bryant?', Answer: 'Los Angeles Lakers'},
    {Question: 'How many NBA championships did Kobe Bryant win?', Answer: '5'},
    {Question: 'How many languages could Kobe Bryant speak fluently?', Answer: '3'},
    {Question: 'What was Kobe Bryant named after?', Answer: 'kobe beef'},
    {Question: 'How many Olympic gold medals did Kobe Bryant recieve?', Answer: '2'},
    {Question: 'Who did Kobe Bryant immitate his game after?', Answer: 'Michael Jordan'},
    {Question: 'How many years did Kobe Bryant play in the NBA?', Answer: '20'},
    ];

/* HELPER FUNCTIONS */

function compare(response, index){
    if (response.value.toString().toLowerCase() === data[index].Answer.toString().toLowerCase()) {
        return true;
      }
      else return false;
}

//function getPlayerLevel(){} this function will return the player's level

//function getPlayerEffiency(){} this function will return the player's shooting effiency

//function getShotsMade(){} this function will return the player's made shots

//function getShotsMissed(){} this function will return the player's missed shots

//this function will let the player attempt a shot
function attemptShot(handlerInput){
    const attributes = handlerInput.attributesManager.getSessionAttributes();
    var result;
    var random = Math.random();
    if(random<attributes.skill){
        result = 'Shot Made!';
        attributes.shotsMade += 1;
        attributes.level += 1;
        handlerInput.attributesManager.setSessionAttributes(attributes);
        
    }
    else{
        result = 'Shot Missed!';
        attributes.shotsMissed += 1;
        handlerInput.attributesManager.setSessionAttributes(attributes);
    }
    return result;
} 
 //this function will give the player a question to answer
function askQuestion(handlerInput){
    const attributes = handlerInput.attributesManager.getSessionAttributes();
    var index = Math.floor(Math.random()*10);
    attributes.index = index;
    handlerInput.attributesManager.setSessionAttributes(attributes);
    return data[index].Question;
}

/* LAMBDA SETUP */
exports.handler = skillBuilder
  .addRequestHandlers(
    LaunchRequestHandler,
    MatchHandler,
    TransferHandler,
    //RepeatHandler,
    //HelpHandler,
    ExitHandler,
    SessionEndedRequestHandler,
    //ErrorHandler,
    AnotherMatchHandler,
    AnotherQuizHandler,
    QuizHandler,
    //DefinitionHandler,
    QuizAnswerHandler,
    //HelpHandler,
    //ExitHandler,
  )
  //.addErrorHandlers(ErrorHandler)
  .lambda();
